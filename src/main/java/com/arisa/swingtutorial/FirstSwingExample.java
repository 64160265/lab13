package com.arisa.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("First JFrame");
        JButton button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        frame.setLayout(null);
        frame.add(button);
        frame.setSize(400,500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
